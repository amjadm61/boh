import { Component, OnInit, Renderer } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  questions: any;
  quizComplted: boolean = false;
  score: any = 0;
  currentQuestionNumber: any = 1;
  totalNumberOfQuestions: any;
  correctAnswerPoint: any = 5;
  currentQuestion: any;
  questionOptions: any;
  showLoader:boolean = true;
  currentQuestionCorrectAnswer: any;
  timeLeft: number = 30;
  loaderColor:any = "#fff";
  interval;

  constructor(private data: DataService, private renderer: Renderer) { }

  ngOnInit() {
    this.getQuestions();
  }

  getQuestions() {
    this.quizComplted = false;
    this.showLoader = true;
    this.score = 0;
    this.currentQuestionNumber = 1;
    this.totalNumberOfQuestions = 0;
    this.data.getQuestions().subscribe(data => {
      let response: any = data;
      this.questions = response.results;
      this.totalNumberOfQuestions = this.questions.length;
      this.getNextQuestions();
      this.showLoader = false;
    });
  }

  validateResult(option, element) {
    this.pauseTimer();
    this.currentQuestionCorrectAnswer = this.questions[this.currentQuestionNumber - 1].correct_answer;
    if (option && element) {
      if (option == this.currentQuestionCorrectAnswer) {
        this.score += this.correctAnswerPoint;
      } else {
        this.renderer.setElementClass(element, "wrong", true);
      }
    }
    setTimeout(() => {
      this.currentQuestionNumber++;
      this.getNextQuestions();
    }, 1500);
  }

  getNextQuestions() {
    if (this.currentQuestionNumber < this.totalNumberOfQuestions + 1) {
      this.timeLeft = 30;
      this.startTimer();
      this.currentQuestion = this.questions[this.currentQuestionNumber - 1].question;
      this.questionOptions = this.questions[this.currentQuestionNumber - 1].incorrect_answers;
      this.questionOptions.push(this.questions[this.currentQuestionNumber - 1].correct_answer);
      this.questionOptions.sort(() => Math.random() - 0.5);
      this.currentQuestionCorrectAnswer = '';
    } else {
      this.quizComplted = true;;
    }
  }

  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.validateResult(false, false);
      }
    }, 1000)
  }

  pauseTimer() {
    clearInterval(this.interval);
  }

}
